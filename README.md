**UI Framework / library - Proof of concept**

Implementing the [Tour of Heroes Demo](http://plnkr.co/edit/?p=preview) in [EmberJS](http://emberjs.com/)

To start the server (runs on port 8080):
> gradle springBoot

The server has 2 endpoints:

1. /heroes - list of 11 heroes
2. /heroes/{id} - return one hero
3. /lots/of/heroes list of 10 000 heroes

The server serves the static files located under: */src/main/resources/static/*

You will need NPM and Ember Cli
> npm install -g ember-cli@2.7

For the EmberJS Project
in ember folder:
> ember server

For test:
> ember test --server
