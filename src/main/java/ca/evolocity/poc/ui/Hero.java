package ca.evolocity.poc.ui;

/**
 * Created by dev on 8/30/16.
 */
public class Hero {

    private int id;
    private String name;

    public Hero(int id, String name) {
        this.id =id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
