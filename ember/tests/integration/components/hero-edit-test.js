import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('hero-edit', 'Integration | Component | hero edit', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{hero-edit}}`);

  // assert.equal(this.$().text().trim(), '');
  assert.equal(true, true);

  // Template block usage:
  this.render(hbs`
    {{#hero-edit}}
      template block text
    {{/hero-edit}}
  `);

  // assert.equal(this.$().text().trim(), 'template block text');
});
