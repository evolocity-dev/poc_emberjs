import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.get('store').findAll('hero');
  },

  actions: {
    detail: function(hero) {
      this.transitionTo('hero', hero.id);
    }
  }
});
