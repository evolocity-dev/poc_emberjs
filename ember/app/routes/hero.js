import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.get('store').findRecord('hero', params.id);
  },

  actions: {
    back: function() {
      console.log('back executed');
      this.transitionTo('heroes');
    },

    save: function() {
      console.log('save executed');
    }
  }
});
